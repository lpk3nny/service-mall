import request from 'axios'

request.defaults.baseURL = 'http://jsonplaceholder.typicode.com/'

export const getTitle = ({ commit, state }) => {
  return request.get('posts/1').then((response) => {
    if (response.statusText === 'OK') {
      commit('TITLE', response.data.title)
    }
  }).catch((error) => {
    console.log(error)
  })
}
