import Vue from 'vue'
import App from './App.vue'
import store from './vuex/store'
import router from './router'
import { sync } from 'vuex-router-sync'
import './assets/css/normalize.css'
import './assets/css/font-awesome.css'
import './assets/css/bootstrap.min.css'
import './styles/project.scss'
import './styles/project-responsive.scss'

sync(store, router)

// Vue.use(BootstrapVue)

const app = new Vue({
  router,
  store,
  ...App
})

export { app, router, store }
