import Vue from 'vue'
import Home from '../components/Home.vue'
import MakeOrder from '../components/MakeOrder.vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: __dirname,
  routes: [
    { path: '/', component: Home },
    { path: '/make-order', component: MakeOrder }
  ]
})

export default router
