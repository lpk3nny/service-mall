var stripInlineComments = require('postcss-strip-inline-comments');
var bem = require('postcss-bem');
var nested = require('postcss-nested');
var opacity = function (css, opts) {
  css.eachDecl(function (decl) {
    if (decl.prop === 'opacity') {
      decl.parent.insertAfter(decl, {
        prop: '-ms-filter',
        value: '"progid:DXImageTransform.Microsoft.Alpha(Opacity=' + (parseFloat(decl.value) * 100) + ')"'
      });
    }
  });
};
var precss = require('precss');
var cssnano = require('cssnano');
var short = require('postcss-short');
var sorting = require('postcss-sorting');


var processors = [
  require('postcss-easy-import'),
  precss,
  bem,
  nested,
  opacity,
  short,
  sorting,
  stripInlineComments,
  cssnano(),

];

module.exports = (ctx) => ({
  'syntax': require('postcss-scss'),
  plugins: processors
})
